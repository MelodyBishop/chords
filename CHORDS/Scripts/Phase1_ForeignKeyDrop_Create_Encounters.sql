
--DROP FKEYS
ALTER TABLE [CHORDS].[ENCOUNTERS] DROP CONSTRAINT [FK_CHORDS_ENCOUNTERS_PROVIDER_SPECIALTY] --RAN THIS
GO
ALTER TABLE [CHORDS].[ENCOUNTERS] DROP CONSTRAINT [FK_CHORDS_ENCOUNTERS_ENCTYPE_LU]
GO
ALTER TABLE [CHORDS].[ENCOUNTERS] DROP CONSTRAINT [FK_CHORDS_ENCOUNTERS_ENCOUNTER_SUBTYPE_LU]
GO
ALTER TABLE [CHORDS].[ENCOUNTERS] DROP CONSTRAINT [FK_CHORDS_ENCOUNTERS_DISCHARGE_STATUS_LU]
GO
ALTER TABLE [CHORDS].[ENCOUNTERS] DROP CONSTRAINT [FK_CHORDS_ENCOUNTERS_DISCHARGE_DISPOSITION_LLU]
GO
ALTER TABLE [CHORDS].[ENCOUNTERS] DROP CONSTRAINT [FK_CHORDS_ENCOUNTERS_DEPARTMENT_LU]
GO
ALTER TABLE [CHORDS].[ENCOUNTERS] DROP CONSTRAINT [FK_CHORDS_ENCOUNTERS_DEMOGRAPHICS] ---RAN THIS
GO
ALTER TABLE [CHORDS].[ENCOUNTERS] DROP CONSTRAINT [FK_CHORDS_ENCOUNTERS_ADMITTING_SOURCE_LU]
GO
ALTER TABLE [CHORDS].[ENCOUNTERS] DROP CONSTRAINT [DF__ENCOUNTER__DEPAR__2645B050]
GO
ALTER TABLE [CHORDS].[ENCOUNTERS] DROP CONSTRAINT [DF__ENCOUNTER__ADMIT__25518C17]
GO
ALTER TABLE [CHORDS].[ENCOUNTERS] DROP CONSTRAINT [DF__ENCOUNTER__DISCH__245D67DE]
GO
ALTER TABLE [CHORDS].[ENCOUNTERS] DROP CONSTRAINT [DF__ENCOUNTER__DISCH__236943A5]
GO
ALTER TABLE [CHORDS].[ENCOUNTERS] DROP CONSTRAINT [DF__ENCOUNTER__FACIL__22751F6C]
GO
--ADD FKEYS

ALTER TABLE [CHORDS].[ENCOUNTERS] ADD  DEFAULT ('UNK') FOR [FACILITY_CODE]
GO

ALTER TABLE [CHORDS].[ENCOUNTERS] ADD  DEFAULT ('U') FOR [DISCHARGE_DISPOSITION]
GO

ALTER TABLE [CHORDS].[ENCOUNTERS] ADD  DEFAULT ('UN') FOR [DISCHARGE_STATUS]
GO

ALTER TABLE [CHORDS].[ENCOUNTERS] ADD  DEFAULT ('UN') FOR [ADMITTING_SOURCE]
GO

ALTER TABLE [CHORDS].[ENCOUNTERS] ADD  DEFAULT ('UNK') FOR [DEPARTMENT]
GO

ALTER TABLE [CHORDS].[ENCOUNTERS]  WITH CHECK ADD  CONSTRAINT [FK_CHORDS_ENCOUNTERS_ADMITTING_SOURCE_LU] FOREIGN KEY([ADMITTING_SOURCE])
REFERENCES [CHORDS].[ADMITTING_SOURCE_LU] ([ABBREVIATION])
GO

ALTER TABLE [CHORDS].[ENCOUNTERS] CHECK CONSTRAINT [FK_CHORDS_ENCOUNTERS_ADMITTING_SOURCE_LU]
GO

ALTER TABLE [CHORDS].[ENCOUNTERS]  WITH CHECK ADD  CONSTRAINT [FK_CHORDS_ENCOUNTERS_DEMOGRAPHICS] FOREIGN KEY([PERSON_ID])
REFERENCES [CHORDS].[DEMOGRAPHICS] ([PERSON_ID])
GO

ALTER TABLE [CHORDS].[ENCOUNTERS] CHECK CONSTRAINT [FK_CHORDS_ENCOUNTERS_DEMOGRAPHICS]
GO

ALTER TABLE [CHORDS].[ENCOUNTERS]  WITH CHECK ADD  CONSTRAINT [FK_CHORDS_ENCOUNTERS_DEPARTMENT_LU] FOREIGN KEY([DEPARTMENT])
REFERENCES [CHORDS].[DEPARTMENT_LU] ([ABBREVIATION])
GO

ALTER TABLE [CHORDS].[ENCOUNTERS] CHECK CONSTRAINT [FK_CHORDS_ENCOUNTERS_DEPARTMENT_LU]
GO

ALTER TABLE [CHORDS].[ENCOUNTERS]  WITH CHECK ADD  CONSTRAINT [FK_CHORDS_ENCOUNTERS_DISCHARGE_DISPOSITION_LLU] FOREIGN KEY([DISCHARGE_DISPOSITION])
REFERENCES [CHORDS].[DISCHARGE_DISPOSITION_LU] ([ABBREVIATION])
GO

ALTER TABLE [CHORDS].[ENCOUNTERS] CHECK CONSTRAINT [FK_CHORDS_ENCOUNTERS_DISCHARGE_DISPOSITION_LLU]
GO

ALTER TABLE [CHORDS].[ENCOUNTERS]  WITH CHECK ADD  CONSTRAINT [FK_CHORDS_ENCOUNTERS_DISCHARGE_STATUS_LU] FOREIGN KEY([DISCHARGE_STATUS])
REFERENCES [CHORDS].[DISCHARGE_STATUS_LU] ([ABBREVIATION])
GO

ALTER TABLE [CHORDS].[ENCOUNTERS] CHECK CONSTRAINT [FK_CHORDS_ENCOUNTERS_DISCHARGE_STATUS_LU]
GO

ALTER TABLE [CHORDS].[ENCOUNTERS]  WITH CHECK ADD  CONSTRAINT [FK_CHORDS_ENCOUNTERS_ENCOUNTER_SUBTYPE_LU] FOREIGN KEY([ENCOUNTER_SUBTYPE])
REFERENCES [CHORDS].[ENCOUNTER_SUBTYPE_LU] ([ABBREVIATION])
GO

ALTER TABLE [CHORDS].[ENCOUNTERS] CHECK CONSTRAINT [FK_CHORDS_ENCOUNTERS_ENCOUNTER_SUBTYPE_LU]
GO

ALTER TABLE [CHORDS].[ENCOUNTERS]  WITH CHECK ADD  CONSTRAINT [FK_CHORDS_ENCOUNTERS_ENCTYPE_LU] FOREIGN KEY([ENCTYPE])
REFERENCES [CHORDS].[ENCTYPE_LU] ([ABBREVIATION])
GO

ALTER TABLE [CHORDS].[ENCOUNTERS] CHECK CONSTRAINT [FK_CHORDS_ENCOUNTERS_ENCTYPE_LU]
GO

ALTER TABLE [CHORDS].[ENCOUNTERS]  WITH CHECK ADD  CONSTRAINT [FK_CHORDS_ENCOUNTERS_PROVIDER_SPECIALTY] FOREIGN KEY([PROVIDER])
REFERENCES [CHORDS].[PROVIDER_SPECIALTY] ([PROVIDER])
GO

ALTER TABLE [CHORDS].[ENCOUNTERS] CHECK CONSTRAINT [FK_CHORDS_ENCOUNTERS_PROVIDER_SPECIALTY]
GO


