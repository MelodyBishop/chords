
USE [JCMH]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--
--
--/* ************************************************************************************************************************************************************************************************************************************************ 
--Name:		    JC_A_SP_Service_Merge
--Author:	    Mary Steffeck, Jefferson Center
--Created:	    February 2017
--Description:  This proc creates the TIER and Avatar side of the JCMH__SERVICE table together and add indexes
--              
--Modified:	    3/23/2017, MLS - RVU is not populated, Base_RVU needed to be converted to float
--              8/31/2017, MLS - Add new column, Indigent.  This column is calculated with Avatar data only.
--              10/26/2017, MLS - Add new columns, current_program, current_care_coord, episode_program_value,
--                                episode_program_code, admit_date and discharge_date
--
--*************************************************************************************************************************************/
AL--TER    procedure [dbo].[JC_A_SP_Service_Merge]
as


drop table jcmh.dbo.JCMH__SERVICE

-- -- -- Add TIER services, first
select inquirykey, clientkey , convert(varchar(20),chargekey) as chargekey, arevclientkey, 
       fullname  as fullname, namel  as namel,
       datesvc, convert(varchar(10), datesvc,112) as datesvc_ymd, 
       timesvc   as timesvc, datesvc_ym, intproccode,
       descr  as Descr, PgmName  PgmName,
       PgmNum, DeptName  as DeptName, JCMH_cpt_code,
       Flexservcode, CostClass, UnitCost,
       PrimaryPayor  as PrimaryPayor, PolicyNum  as PolicyNum,
       PrimaryDx  as PrimaryDx, GAF, [Dx Grouping],
       JCMHID, ClinicianName  as ClinicianName, StaffKey, 
       Credentials  as Credentials, PlaceOfService  as PlaceOfService,
       Duration, Zip  as Zip, DOB, Age, AgeCategory, 
       Gender  as Gender, County  as County, 
       City  as City, NumofDep, Target, 
       PayorType  as PayorType, FinalSaveDate, convert(varchar(10), FinalSaveDate,112) as FinalSave_ymd,
       FinalSave_ym, IsEncounter, IsEnrolled, MedicaidID, IsEligible, 
       EligCategory, CCARType, Base_RVU, Sum_RVU, Prockey, 
       RVUISFacility, VO_CPT_Code, Income, Units,
       Charged_Amt, PayorPlanKey, convert(varchar(20),opdocid) as opdocid, Consolidation, Indigent,
       -- add 6 new columns, MLS 10/26/2017
       Current_Program, Current_Care_Coord,
       Episode_Program_Value, Episode_Program_Code, Admit_Date, Discharge_Date
       
into dbo.JCMH__SERVICE
from dbo.JCMH__SERVICE_TIER
where UPPER(namel) <> 'TEST';

---- -- add new column called Indigent, which will have Avatar date only
--ALTER TABLE dbo.JCMH__SERVICE_TIER ADD Indigent varchar(10) NULL;
--go

---- -- add new column called Indigent, which will have Avatar date only
--ALTER TABLE dbo.JCMH__SERVICE ADD Indigent varchar(10) NULL;
--go


-- then add Avatar services, last
insert into dbo.JCMH__SERVICE
select convert(int,inquirykey) as inquirykey, convert(int,clientkey) as clientkey, 
       convert(varchar(20),chargekey) as chargekey, convert(varchar(10),arevclientkey) as arevclientkey, 
       substring(fullname,1,41)  as fullname,
       substring(namel,1,30)  as namel, datesvc, datesvc_ymd, 
       case
         when timesvc = ' ' then '00:00:00:000'
         else substring(convert(varchar(12), timesvc,14),1,5) + ':00:000'
       end as timesvc,
       datesvc_ym, convert (varchar(10),intproccode) as intproccode,
       descr  as Descr, PgmName  as PgmName,
       convert(int,PgmNum) as PgmNum, substring(DeptName,1,40)  as DeptName, substring(JCMH_cpt_code,1,8) as JCMH_cpt_code,
       convert(varchar(10),Flexservcode) as Flexservcode, convert(varchar(10),CostClass) as CostClass, UnitCost,
       PrimaryPayor  as PrimaryPayor, convert(varchar(16),PolicyNum)  as PolicyNum,
       PrimaryDx  as PrimaryDx, GAF, convert(varchar(255),[Dx Grouping]) as [Dx Grouping],
       convert(varchar(10),JCMHID) as JCMHID, ClinicianName  as ClinicianName, convert(int,StaffKey) as StaffKey, 
       Credentials, convert(char(50),PlaceOfService)  as PlaceOfService,
       convert(float,Duration) as Duration, substring(Zip,1,10)  as Zip, DOB, Age, AgeCategory, 
       Gender  as Gender, County  as County,
       City  as City, convert(int,NumofDep) as NumofDep, Target, 
       convert(char(8),PayorType)  as PayorType, FinalSave as FinalSaveDate, FinalSave_ymd,
       FinalSave_ym, convert(char(1),IsEncounter) as IsEncounter, IsEnrolled, MedicaidID, IsEligible, 
       convert(varchar(10),EligCategory) as EligCategory, CCARType, convert(float,Base_RVU) as Base_RVU, Sum_RVU, 
       convert(int,Prockey) as Prockey, 
       convert(char(1),RVUISFacility) as RVUISFacility, convert(varchar(50),VO_CPT_Code) as VO_CPT_Code, Income, 
       convert(float,Units),
       Charge_Amt as Charged_Amt, PayorPlanKey, opdocid, convert(varchar(25), Consolidation) as Consolidation, 
       Indigent, 
       -- add 6 new columns, MLS 10/26/2017
       Current_Program, Current_Care_Coord,
       Episode_Program_Value, Episode_Program_Code, Admit_Date, Discharge_Date
from dbo.JCMH__Service_Avatar
where UPPER(namel) <> 'TEST';



----- how many notes/services/encounters per month
--select FinalSave_ym, count(FinalSave_ym)
--from jcmh.dbo.JCMH__SERVICE
--group by FinalSave_ym
--order by FinalSave_ym

--select *
--from dbo.JCMH__SERVICE


----
---- add indexes
---- ----  these index are for Diane's reports
-- CREATE NONCLUSTERED INDEX [indx_clientkey] ON [JCMH].[dbo].[JCMH__SERVICE]([ClientKey]);
-- GO
-- CREATE NONCLUSTERED INDEX [indx_inquirykey] ON [JCMH].[dbo].[JCMH__SERVICE]([InquiryKey]);
-- GO