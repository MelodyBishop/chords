
USE [CHORDS]
GO

DROP PROCEDURE STAGING.[JC_A_SP_Create_Staging_Encounter_Avatar]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--execute STAGING.[JC_A_SP_Create_Staging_Encounter_Avatar] --Approx RunTime:  Less than 1 minute
--
--/* ************************************************************************************************************************************************************************************************************************************************ 
--Name:		    JC_A_SP_Create_CHORDS_Encounter_Avatar
--Author:		Mary Steffeck, JCMH
--Created:	    March 2018
--Description:  This proc will read all chargeslips in TIER and create CHORDS encounters.
--              
--Modified: 4/19/2018 to point to staging to bypass FKey constraint issues while building tables - MB    
--Added masking of the Patid portion of the Enc_ID to show the Person_ID
--
--*************************************************************************************************************************************/
CREATE   procedure STAGING.[JC_A_SP_Create_Staging_Encounter_Avatar]
as


Truncate Table STAGING.Encounters

declare @begindate datetime
set @begindate = '10/1/2016'

--delete from STAGING.Encounters
--select * from STAGING.encounters
Insert into STAGING.Encounters
Select 
Replace(enc_ID,patid,person_id) Enc_ID 
,Person_ID
,ADate
,DDate
,Provider
,Enc_Count
,Drg_Version
,Drg_Value
,EncType
,Encounter_Subtype
,location_code
,Discharge_Disposition
,Discharge_Status
,Admitting_Source
,Department

From
(
select 
	    txhist.id as enc_id,
		cw.patid,
       ISNULL((select top 1 person_id
        from CHORDS.CHORDS.CHORDS_ID 
        where MRN = cw.patid),'JC9999999990') as person_id
	   ,Cast(txhist.date_of_service as date) as adate  -- format to YYYYMMDD
       ,NULL as ddate
       ,ISNULL((select Provider
	    from [CHORDS].[CHORDS].[CHORDS_PROVIDER]
		where convert(int,[Staff_ID]) = convert(int,txhist.PROVIDER_ID)),'JC9999999999') as 'Provider'
	   ,1 as Enc_Count
       ,NULL as Drg_Version
       ,NULL as Drg_Value
      , case
			when cw.service_charge_code in (407,930,931,932) then 'TE'  -- phone procedures
			 else 'OE'  -- other encounter
		end as enctype
       ,'OT' as encounter_subtype  -- other non-hospital
       ,cw.location_code --as Facility_Code
       ,'U' as discharge_disposition
       ,'UN' as discharge_status
       ,'UN' as admitting_source
       ,'MH' as department

from Avatardw.SYSTEM.billing_tx_history txhist
  inner join Avatardw.cwssystem.cw_patient_notes cw
			 on txhist.JOIN_TO_TX_HISTORY = cw.JOIN_TO_TX_HISTORY and txhist.patid = cw.patid
where txhist.facility = '1' 
  and txhist.date_of_service >= @begindate
  and substring(cw.practitioner_name,1,5) <> 'Test,'  -- omit TEST clinicians
  and substring(txhist.v_patient_name,1,5) <> 'Test,'  -- omit TEST clients
  and substring(txhist.v_patient_name,1,11) <> 'GROUP,GROUP'  -- omit TEST clients
) sub
 
GO

 
