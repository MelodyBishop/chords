/****** Object:  Table [CHORDS].[RACETemp]    Script Date: 3/12/2018 7:12:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [CHORDS].[RACETemp](
	[patid] [nvarchar](12) NOT NULL,
	[Race1] [nvarchar](max) NULL,
	[Race2] [nvarchar](max) NULL,
	[Race3] [nvarchar](max) NULL,
	[Race4] [nvarchar](max) NULL,
	[Race5] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
