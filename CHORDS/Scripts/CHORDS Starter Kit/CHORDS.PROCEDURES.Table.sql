/****** Object:  Table [CHORDS].[PROCEDURES]    Script Date: 3/12/2018 7:12:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [CHORDS].[PROCEDURES](
	[PROCEDURES_ID] [int] IDENTITY(1,1) NOT NULL,
	[ENC_ID] [nvarchar](36) NOT NULL,
	[PROCDATE] [date] NOT NULL,
	[PERFORMINGPROVIDER] [nvarchar](36) NOT NULL,
	[ORIGPX] [nvarchar](10) NULL,
	[PX] [nvarchar](10) NOT NULL,
	[PX_CODETYPE] [nvarchar](2) NOT NULL,
	[ROW_ID] [nvarchar](8) NULL,
	[PERSON_ID] [nvarchar](12) NOT NULL,
	[PROVIDER] [nvarchar](36) NOT NULL,
	[ADATE] [date] NOT NULL,
	[ENCTYPE] [nvarchar](2) NOT NULL,
	[PXCNT] [numeric](7, 0) NOT NULL,
	[CPTMOD1] [nvarchar](2) NULL,
	[CPTMOD2] [nvarchar](2) NULL,
	[CPTMOD3] [nvarchar](2) NULL,
PRIMARY KEY NONCLUSTERED 
(
	[PROCEDURES_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [CHORDS].[PROCEDURES] ADD  DEFAULT ((1)) FOR [PXCNT]
GO
ALTER TABLE [CHORDS].[PROCEDURES]  WITH NOCHECK ADD  CONSTRAINT [FK_CHORDS_PROCEDURES_DEMOGRAPHICS] FOREIGN KEY([PERSON_ID])
REFERENCES [CHORDS].[DEMOGRAPHICS] ([PERSON_ID])
GO
ALTER TABLE [CHORDS].[PROCEDURES] NOCHECK CONSTRAINT [FK_CHORDS_PROCEDURES_DEMOGRAPHICS]
GO
ALTER TABLE [CHORDS].[PROCEDURES]  WITH CHECK ADD  CONSTRAINT [FK_CHORDS_PROCEDURES_ENCOUNTERS] FOREIGN KEY([ENC_ID])
REFERENCES [CHORDS].[ENCOUNTERS] ([ENC_ID])
GO
ALTER TABLE [CHORDS].[PROCEDURES] CHECK CONSTRAINT [FK_CHORDS_PROCEDURES_ENCOUNTERS]
GO
ALTER TABLE [CHORDS].[PROCEDURES]  WITH CHECK ADD  CONSTRAINT [FK_CHORDS_PROCEDURES_PROVIDER_SPECIALTY] FOREIGN KEY([PROVIDER])
REFERENCES [CHORDS].[PROVIDER_SPECIALTY] ([PROVIDER])
GO
ALTER TABLE [CHORDS].[PROCEDURES] CHECK CONSTRAINT [FK_CHORDS_PROCEDURES_PROVIDER_SPECIALTY]
GO
ALTER TABLE [CHORDS].[PROCEDURES]  WITH CHECK ADD  CONSTRAINT [FK_CHORDS_PROCEDURES_PROVIDER_SPECIALTY2] FOREIGN KEY([PERFORMINGPROVIDER])
REFERENCES [CHORDS].[PROVIDER_SPECIALTY] ([PROVIDER])
GO
ALTER TABLE [CHORDS].[PROCEDURES] CHECK CONSTRAINT [FK_CHORDS_PROCEDURES_PROVIDER_SPECIALTY2]
GO
ALTER TABLE [CHORDS].[PROCEDURES]  WITH CHECK ADD  CONSTRAINT [FK_CHORDS_PROCEDURES_PX_CODETYPE_LU] FOREIGN KEY([PX_CODETYPE])
REFERENCES [CHORDS].[PX_CODETYPE_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[PROCEDURES] CHECK CONSTRAINT [FK_CHORDS_PROCEDURES_PX_CODETYPE_LU]
GO
