/****** Object:  Table [CHORDS].[EVERNDC]    Script Date: 3/12/2018 7:12:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [CHORDS].[EVERNDC](
	[NDC] [nvarchar](11) NOT NULL,
	[GENERIC] [nvarchar](105) NOT NULL,
	[BRAND] [nvarchar](105) NULL,
	[GPI] [nvarchar](14) NULL,
	[AHFS1] [nvarchar](8) NULL,
	[AHFS2] [nvarchar](8) NULL,
	[AHFS3] [nvarchar](8) NULL,
	[AHFS4] [nvarchar](8) NULL,
	[AHFS5] [nvarchar](8) NULL,
	[AHFS6] [nvarchar](8) NULL,
	[AHFS7] [nvarchar](8) NULL,
 CONSTRAINT [PK_CHORDS_EVERNDC] PRIMARY KEY CLUSTERED 
(
	[NDC] ASC,
	[GENERIC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
