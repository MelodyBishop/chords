/****** Object:  Table [CHORDS].[CHORDS_ID]    Script Date: 3/12/2018 7:12:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CHORDS].[CHORDS_ID](
	[MRN] [varchar](20) NULL,
	[PERSON_ID] [varchar](12) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
