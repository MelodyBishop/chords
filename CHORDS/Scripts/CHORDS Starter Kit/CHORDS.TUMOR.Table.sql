/****** Object:  Table [CHORDS].[TUMOR]    Script Date: 3/12/2018 7:12:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [CHORDS].[TUMOR](
	[TUMOR_ID] [int] IDENTITY(1,1) NOT NULL,
	[PERSON_ID] [nvarchar](12) NOT NULL,
	[SEQUENCE] [nvarchar](2) NOT NULL,
	[DXDATE] [date] NOT NULL,
	[ICDOSITE] [nvarchar](4) NOT NULL,
	[STAGEGEN] [nchar](1) NOT NULL,
	[SS1977] [nchar](1) NULL,
	[SS2000] [nchar](1) NULL,
	[STAGEAJ] [nvarchar](4) NOT NULL,
	[AJCC_ED] [nchar](1) NULL,
	[AJCC_GRP] [nvarchar](2) NULL,
	[AJCC_FULL_DER] [nvarchar](20) NULL,
	[MORPH] [nvarchar](4) NOT NULL,
	[BEHAV] [nchar](1) NOT NULL,
	[GRADE] [nchar](1) NOT NULL,
	[DXYEAR] [numeric](4, 0) NOT NULL,
	[DXAGE] [numeric](3, 0) NOT NULL,
	[BDATE] [date] NOT NULL,
	[GENDER] [nchar](1) NOT NULL,
	[RACE1] [nvarchar](2) NOT NULL,
	[RACE2] [nvarchar](2) NOT NULL,
	[RACE3] [nvarchar](2) NOT NULL,
	[RACE4] [nvarchar](2) NOT NULL,
	[RACE5] [nvarchar](2) NOT NULL,
	[HISPANIC] [nchar](1) NOT NULL,
	[CLASS] [nvarchar](2) NOT NULL,
	[VITAL] [nchar](1) NOT NULL,
	[DCAUSE] [nvarchar](6) NULL,
	[DOD] [date] NULL,
	[DT_FU] [date] NULL,
	[LATERALITY] [nchar](1) NOT NULL,
	[IDPLAN] [nvarchar](2) NOT NULL,
	[DCNFRM] [nchar](1) NOT NULL,
	[DSTZ] [nvarchar](3) NULL,
	[DAJC1T_P] [nvarchar](5) NULL,
	[DAJC1N_P] [nvarchar](5) NULL,
	[DAJC1M_P] [nvarchar](5) NULL,
	[DAJC1T_C] [nvarchar](5) NULL,
	[DAJC1N_C] [nvarchar](5) NULL,
	[DAJC1M_C] [nvarchar](5) NULL,
	[DSRG_FAC] [nvarchar](2) NOT NULL,
	[DRAD_FAC] [nvarchar](2) NOT NULL,
	[DCHM_FAC] [nvarchar](2) NOT NULL,
	[DHRM_FAC] [nvarchar](2) NOT NULL,
	[DIMM_FAC] [nvarchar](2) NOT NULL,
	[DOTH_FAC] [nvarchar](2) NOT NULL,
	[DNDI] [nvarchar](2) NOT NULL,
	[DNDX] [nvarchar](2) NOT NULL,
	[DTMRK1] [nchar](1) NULL,
	[DTMRK2] [nchar](1) NULL,
	[DTMRK3] [nchar](1) NULL,
	[CLN_STG] [nvarchar](4) NOT NULL,
	[EOD] [nvarchar](12) NULL,
	[DT_SURG] [date] NULL,
	[DT_CHEMO] [date] NULL,
	[DT_HORM] [date] NULL,
	[DT_RAD] [date] NULL,
	[DT_BRM] [date] NULL,
	[DT_OTH] [date] NULL,
	[R_N_SURG] [nchar](1) NOT NULL,
	[R_N_CHEMO] [nvarchar](2) NOT NULL,
	[R_N_HORM] [nchar](1) NOT NULL,
	[R_N_RAD] [nchar](1) NOT NULL,
	[R_N_BRM] [nchar](1) NOT NULL,
	[R_N_OTH] [nchar](1) NOT NULL,
	[DSRG_SUM] [nvarchar](2) NOT NULL,
	[DRAD_SUM] [nvarchar](2) NOT NULL,
	[DCHM_SUM] [nvarchar](2) NOT NULL,
	[DHRM_SUM] [nvarchar](2) NOT NULL,
	[DIMM_SUM] [nvarchar](2) NOT NULL,
	[DOTH_SUM] [nvarchar](2) NOT NULL,
	[CS_SZ] [nvarchar](3) NULL,
	[CS_EXT] [nvarchar](3) NULL,
	[CS_NODES] [nvarchar](3) NULL,
	[CS_NODES_EVAL] [nchar](1) NULL,
	[CS_METS] [nvarchar](2) NULL,
	[CS_METS_EVAL] [nchar](1) NULL,
	[SSF1] [nvarchar](3) NULL,
	[SSF2] [nvarchar](3) NULL,
	[SSF3] [nvarchar](3) NULL,
	[SSF4] [nvarchar](3) NULL,
	[SSF5] [nvarchar](3) NULL,
	[SSF6] [nvarchar](3) NULL,
	[SSF7] [nvarchar](3) NULL,
	[SSF8] [nvarchar](3) NULL,
	[SSF9] [nvarchar](3) NULL,
	[SSF10] [nvarchar](3) NULL,
	[SSF11] [nvarchar](3) NULL,
	[SSF12] [nvarchar](3) NULL,
	[SSF13] [nvarchar](3) NULL,
	[SSF14] [nvarchar](3) NULL,
	[SSF15] [nvarchar](3) NULL,
	[SSF16] [nvarchar](3) NULL,
	[SSF17] [nvarchar](3) NULL,
	[SSF18] [nvarchar](3) NULL,
	[SSF19] [nvarchar](3) NULL,
	[SSF20] [nvarchar](3) NULL,
	[SSF21] [nvarchar](3) NULL,
	[SSF22] [nvarchar](3) NULL,
	[SSF23] [nvarchar](3) NULL,
	[SSF24] [nvarchar](3) NULL,
	[SSF25] [nvarchar](3) NULL,
	[PAL_FAC] [nchar](1) NOT NULL,
	[PAL_SUM] [nchar](1) NOT NULL,
	[DER_T6] [nvarchar](3) NULL,
	[DER_T6_D] [nchar](1) NULL,
	[DER_N6] [nvarchar](3) NULL,
	[DER_N6_D] [nchar](1) NULL,
	[DER_M6] [nvarchar](3) NULL,
	[DER_M6_D] [nchar](1) NULL,
	[DER_T7] [nvarchar](3) NULL,
	[DER_T7_D] [nchar](1) NULL,
	[DER_N7] [nvarchar](3) NULL,
	[DER_N7_D] [nchar](1) NULL,
	[DER_M7] [nvarchar](3) NULL,
	[DER_M7_D] [nchar](1) NULL,
	[DER_SS2000F] [nchar](1) NULL,
	[RECUR_DT] [datetime] NULL,
	[RECUR_TYPE] [nvarchar](2) NOT NULL,
	[RECUR_FL] [nvarchar](2) NULL,
	[DATA_SOURCE] [nvarchar](4) NULL,
PRIMARY KEY NONCLUSTERED 
(
	[TUMOR_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [CHORDS].[TUMOR]  WITH NOCHECK ADD  CONSTRAINT [FK_CHORDS_TUMOR_DEMOGRAPHICS] FOREIGN KEY([PERSON_ID])
REFERENCES [CHORDS].[DEMOGRAPHICS] ([PERSON_ID])
GO
ALTER TABLE [CHORDS].[TUMOR] NOCHECK CONSTRAINT [FK_CHORDS_TUMOR_DEMOGRAPHICS]
GO
ALTER TABLE [CHORDS].[TUMOR]  WITH CHECK ADD  CONSTRAINT [FK_CHORDS_TUMOR_RACE_LU] FOREIGN KEY([RACE1])
REFERENCES [CHORDS].[RACE_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[TUMOR] CHECK CONSTRAINT [FK_CHORDS_TUMOR_RACE_LU]
GO
ALTER TABLE [CHORDS].[TUMOR]  WITH CHECK ADD  CONSTRAINT [FK_CHORDS_TUMOR_RACE_LU2] FOREIGN KEY([RACE2])
REFERENCES [CHORDS].[RACE_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[TUMOR] CHECK CONSTRAINT [FK_CHORDS_TUMOR_RACE_LU2]
GO
ALTER TABLE [CHORDS].[TUMOR]  WITH CHECK ADD  CONSTRAINT [FK_CHORDS_TUMOR_RACE_LU3] FOREIGN KEY([RACE3])
REFERENCES [CHORDS].[RACE_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[TUMOR] CHECK CONSTRAINT [FK_CHORDS_TUMOR_RACE_LU3]
GO
ALTER TABLE [CHORDS].[TUMOR]  WITH CHECK ADD  CONSTRAINT [FK_CHORDS_TUMOR_RACE_LU4] FOREIGN KEY([RACE4])
REFERENCES [CHORDS].[RACE_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[TUMOR] CHECK CONSTRAINT [FK_CHORDS_TUMOR_RACE_LU4]
GO
ALTER TABLE [CHORDS].[TUMOR]  WITH CHECK ADD  CONSTRAINT [FK_CHORDS_TUMOR_RACE_LU5] FOREIGN KEY([RACE5])
REFERENCES [CHORDS].[RACE_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[TUMOR] CHECK CONSTRAINT [FK_CHORDS_TUMOR_RACE_LU5]
GO
