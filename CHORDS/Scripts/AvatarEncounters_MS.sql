USE CHORDS

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--
--
--/* ************************************************************************************************************************************************************************************************************************************************ 
--Name:		    JC_A_SP_Create_CHORDS_Encounter_Avatar
--Author:		Mary Steffeck, JCMH
--Created:	    March 2018
--Description:  This proc will read all chargeslips in TIER and create CHORDS encounters.
--              
--Modified:     Updated for JCVS2/Cloud 
--
--
--*************************************************************************************************************************************/
CREATE    procedure [dbo].[JC_A_SP_Create_CHORDS_Encounter_Avatar]
as


declare @begindate datetime
set @begindate = '10/1/2016'

select top 10
       (select top 1 person_id
        from CHORDS.CHORDS.CHORDS_ID 
        where MRN = cw.patid) as person_id,

       cw.patid,

       txhist.date_of_service, 
	   replace(convert(varchar(10),txhist.date_of_service,101),'/','') as adate,  -- format to YYYYMMDD

       txhist.id as enc_id,
       NULL as ddate,

	   txhist.PROVIDER_ID,
       cw.practitioner_id, cw.practitioner_name, -- provider
	   (select Provider
	    from [CHORDS].[CHORDS].[CHORDS_PROVIDER]
		where convert(int,[Staff_ID]) = convert(int,txhist.PROVIDER_ID)) as 'Provider',

       1 as Enc_Count,
       NULL as Drg_Version,
       NULL as Drg_Value,

       case
         when cw.service_charge_code in (407,930,931,932) then 'TE'  -- phone procedures
         else 'OE'  -- other encounter
       end as enctype,
       'OT' as encounter_subtype,  -- other non-hospital

       cw.location_code as Facility_Code, cw.location_value,  

       'U' as discharge_disposition,
       'UN' as discharge_status,
       'UN' as admitting_source,
       'MH' as department


--from avatardw.SYSTEM.billing_tx_history txhist
from Avatardw.SYSTEM.billing_tx_history txhist
  inner join Avatardw.cwssystem.cw_patient_notes cw
			 on txhist.JOIN_TO_TX_HISTORY = cw.JOIN_TO_TX_HISTORY
			 and txhist.patid = cw.patid
where txhist.facility = '1' 
  and txhist.date_of_service >= @begindate
  and substring(cw.practitioner_name,1,5) <> 'Test,'  -- omit TEST clinicians
  and substring(txhist.v_patient_name,1,5) <> 'Test,'  -- omit TEST clients

  

                         
