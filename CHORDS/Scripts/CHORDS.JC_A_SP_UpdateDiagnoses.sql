

USE [CHORDS]
GO


DROP TABLE STAGING.[DIAGNOSES]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE STAGING.[DIAGNOSES](
	--[DIAGNOSES_ID] [int] IDENTITY(1,1) NOT NULL, --there is no diagnosis ID in specifications
	[ENC_ID] [nvarchar](36) NOT NULL,
	[DX] [nvarchar](10) NOT NULL,
	[ADATE] [date] NOT NULL,
	[DIAGPROVIDER] [nvarchar](36) NOT NULL,
	[PERSON_ID] [nvarchar](12) NOT NULL,
	[ENCTYPE] [nvarchar](2) NOT NULL,
	[PROVIDER] [nvarchar](36) NOT NULL,
	[ORIGDX] [nvarchar](10) NOT NULL,
	[DX_CODETYPE] [nvarchar](2) NOT NULL,
	[PRINCIPAL_DX] [nchar](1) NOT NULL,
	[PRIMARY_DX] [nchar](1) NOT NULL
) ON [PRIMARY]
GO



