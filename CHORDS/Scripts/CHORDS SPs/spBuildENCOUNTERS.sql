USE [CHORDSlive]
GO
/****** Object:  StoredProcedure [dbo].[spBuildENCOUNTERS]    Script Date: 9/14/2017 10:26:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spBuildENCOUNTERS]
AS

/* 
=================================================
Object Name...: spBuildENCOUNTERS
Purposes......: Build CHORDS VDW P1 Table ENCOUNTERS
Author........: CJ McKinney
Create date...: 2017-5-30

Modified 2017-9-10 by CJ McKinney
Updated to Conform to VDW 3.1 specifications
*/

IF OBJECT_ID('tempdb.dbo.#ENCOUNTERS','U') is not null DROP TABLE #ENCOUNTERS;
CREATE TABLE #ENCOUNTERS
(
ENC_ID                  CHAR(36) NOT NULL,
PERSON_ID               CHAR(12) NOT NULL,
ADATE                   DATE NOT NULL,
DDATE                   DATE,
PROVIDER                CHAR(36) NOT NULL,     
ENC_COUNT               NUMERIC(3) NOT NULL,
DRG_VERSION             CHAR(1),
DRG_VALUE               CHAR(4),
ENCTYPE                 CHAR(2) NOT NULL,
ENCOUNTER_SUBTYPE       CHAR(2) NOT NULL,
FACILITY_CODE           CHAR(36) NOT NULL,
DISCHARGE_DISPOSITION   CHAR(1),
DISCHARGE_STATUS        CHAR(2),
ADMITTING_SOURCE        CHAR(2),
DEPARTMENT              CHAR(4),

CONSTRAINT pk_ENCOUNTERS PRIMARY KEY (ENC_ID)
);


insert into #ENCOUNTERS
select

ENC_ID = [Service ID],
PERSON_ID = i.[CHORDS ID],
ADATE = Date,
DDATE = NULL,
PROVIDER = RIGHT(s.Staff,4),
ENC_COUNT = 1,
DRG_VERSION = NULL, --This is for hospital admissions
DRG_VALUE = NULL, --This is for hospital admissions
ENCTYPE = CASE WHEN [Service Type] like '%residential%' THEN 'IP' WHEN [Mode of Contact] = 'Face-to-Face' THEN 'AV' WHEN [Mode of Contact] = 'Telephone' THEN 'TE' WHEN [Mode of Contact] = 'Written' THEN 'EM' ELSE 'OE' END,
ENCOUNTER_SUBTYPE = CASE WHEN [Service Type] like '%residential%' THEN 'AI' WHEN [Mode of Contact] = 'Face-to-Face' THEN 'OC' ELSE 'OT' END,
FACILITY_CODE = s.[Place of Service Code],
DISCHARGE_DISPOSITION = 'U',
DISCHARGE_STATUS ='UN',
ADMITTING_SOURCE = 'UN',
DEPARTMENT = 'MH' --Update to include others as applicable
from Reporting.dbo.Services s
LEFT JOIN Reporting.dbo.[Staff Summary] ss ON s.Staff = ss.Staff
LEFT JOIN Staging.chords.identifiers i on i.[Consumer ID] = s.[Consumer ID]
WHERE DAte >= '10/1/2015' and [Counts Towards Census?] = 'Yes' and [Contract Compliance Category] <> 'Never Billable' and [Service role] = 'Primary' and i.[CHORDS ID] is not null
;


IF OBJECT_ID('dbo.ENCOUNTERS','U') is not null TRUNCATE TABLE dbo.ENCOUNTERS;


INSERT INTO dbo.ENCOUNTERS
SELECT *
FROM #ENCOUNTERS
WHERE PERSON_ID is not null
;
