USE [CHORDSlive]
GO
/****** Object:  StoredProcedure [dbo].[spRecreateCHORDS_VDW]    Script Date: 9/14/2017 10:26:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[spRecreateCHORDS_VDW]
AS

/* 
=================================================
Object Name...: spRecreateCHORDS_VDW
Purposes......: DELETE and Recreate all CHORDS VDW tables
Only use this script if you need to wipe everything away and recreate the entire VDW
Author........: CJ McKinney
Create date...: 2017-9-12
*/


--DELETE FOREIGN KEYS and CONTRAINTS
WHILE(EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_TYPE = 'FOREIGN KEY' AND CONSTRAINT_NAME LIKE '%FK_CHORDS%'))
BEGIN
    DECLARE @sql_alterTable_fk NVARCHAR(2000)

    SELECT  TOP 1 @sql_alterTable_fk = ('ALTER TABLE ' + TABLE_SCHEMA + '.[' + TABLE_NAME + '] DROP CONSTRAINT [' + CONSTRAINT_NAME + ']')
    FROM    INFORMATION_SCHEMA.TABLE_CONSTRAINTS
    WHERE   CONSTRAINT_TYPE = 'FOREIGN KEY'
            AND CONSTRAINT_NAME LIKE '%FK_CHORDS%'

    EXEC (@sql_alterTable_fk)
END
;

--DROP Tables
drop table IF EXISTS ABN_IND_LU
drop table IF EXISTS ADMITTING_SOURCE_LU
drop table IF EXISTS BP_TYPE_LU
drop table IF EXISTS CAUSETYPE_LU
drop table IF EXISTS CODETYPE_LU
drop table IF EXISTS CONFIDENCE_LU
drop table IF EXISTS DEPARTMENT_LU
drop table IF EXISTS DISCHARGE_DISPOSITION_LU
drop table IF EXISTS DISCHARGE_STATUS_LU
drop table IF EXISTS DTIMPUTE_LU
drop table IF EXISTS DX_CODETYPE_LU
drop table IF EXISTS ENCOUNTER_SUBTYPE_LU
drop table IF EXISTS ENCTYPE_LU
drop table IF EXISTS ENROLLMENT_BASIS_LU
drop table IF EXISTS GENDER_LU
drop table IF EXISTS LANG_USAGE_LU
drop table IF EXISTS MODIFIER_LU
drop table IF EXISTS ONC_SMOKING_STATUS_LU
drop table IF EXISTS POSITION_LU
drop table IF EXISTS PRIMARY_DX_LU
drop table IF EXISTS PRINCIPAL_DX_LU
drop table IF EXISTS PROVIDER_TYPE_LU
drop table IF EXISTS PT_LOC_LU
drop table IF EXISTS PX_CODETYPE_LU
drop table IF EXISTS RACE_LU
drop table IF EXISTS RESULT_LOC_LU
drop table IF EXISTS RX_BASIS_LU
drop table IF EXISTS RX_FREQUENCY_LU
drop table IF EXISTS RX_QUANTITY_UNIT_LU
drop table IF EXISTS SEXUALLY_ACTV_LU
drop table IF EXISTS SPECIMEN_SOURCE_LU
drop table IF EXISTS STAT_LU
drop table IF EXISTS TOBACCO_USER_LU
drop table IF EXISTS YNQXU_LU
drop table IF EXISTS YNU_LU
drop table IF EXISTS YNXU_LU
drop table IF EXISTS LANGUAGES_ISO_LU
drop table IF EXISTS GENDER_IDENTITY_LU
drop table IF EXISTS SEXUAL_ORIENTATION_LU
drop table IF EXISTS COLORADO_COUNTIES
drop table IF EXISTS CENSUS_LOCATION;
drop table IF EXISTS LANGUAGES;
drop table IF EXISTS TUMOR;
drop table IF EXISTS VITAL_SIGNS;
drop table IF EXISTS SOCIAL_HISTORY;
drop table IF EXISTS [PROCEDURES];
drop table IF EXISTS PRESCRIBING;
drop table IF EXISTS PHARMACY;
drop table IF EXISTS LAB_RESULTS;
drop table IF EXISTS ENROLLMENT;
drop table IF EXISTS DIAGNOSES;
drop table IF EXISTS ENCOUNTERS;
drop table IF EXISTS DEMOGRAPHICS;
drop table IF EXISTS CAUSE_OF_DEATH;
drop table IF EXISTS DEATH;
drop table IF EXISTS PROVIDER_SPECIALTY
drop table IF EXISTS EVERNDC;
drop table IF EXISTS CENSUS_DEMOG;


--Create Look-up Tables
EXEC [dbo].[spCreateCHORDSLookUpTables];

--Create primary CHORDS data Tables
EXEC [dbo].[spCreatePrimaryCHORDSTables];

--Create CENSUS_DEMOG table
EXEC [dbo].[spBuildCENSUS_DEMOG];

--Add data to tables
EXEC [dbo].[spInsertCHORDSP1TableData];

--Create Foriegn Keys and Contraints
EXEC [dbo].[spBuildForeignKeysConstraints];

--Index Tables
EXEC [dbo].[spBuildIndexes];






