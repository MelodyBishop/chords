USE [CHORDSlive]
GO
/****** Object:  StoredProcedure [dbo].[spBuildVITAL_SIGNS]    Script Date: 9/14/2017 10:26:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spBuildVITAL_SIGNS]
AS

/* 
=================================================
Object Name...: spBuildVITAL_SIGNS
Purposes......: Build CHORDS VDW P1 Table VITAL_SIGNS
Author........: CJ McKinney
Create date...: 2017-5-30

Modified 2017-9-10 by CJ McKinney
Updated to conform to VDW 3.1 specification
*/

IF OBJECT_ID('tempdb.dbo.#VITAL_SIGNS','U') IS NOT NULL DROP TABLE #VITAL_SIGNS;
CREATE TABLE #VITAL_SIGNS
(
PERSON_ID                         CHAR(12) NOT NULL,
MEASURE_DATE                      DATE NOT NULL,
MEASURE_TIME                      TIME NOT NULL,
ENC_ID                		      CHAR(36) NULL,
ENCTYPE                           CHAR(2) NULL,
HT_RAW                            CHAR(10), 
WT_RAW                            CHAR(10),  
HT                                NUMERIC(7,3),
WT                                NUMERIC(8,4),
BMI_RAW                           CHAR(5),     
DIASTOLIC                         NUMERIC(3), 
SYSTOLIC                          NUMERIC(3),
DIASTOLIC_RAW                     CHAR(7),
SYSTOLIC_RAW                      CHAR(7), 
BP_TYPE                           CHAR(1),
POSITION                          CHAR(1),
HEAD_CIR_RAW                      CHAR(6),
RESPIR_RAW                        CHAR(6),
TEMP_RAW                          CHAR(6),  
PULSE_RAW                         CHAR(6)
,
CONSTRAINT pk_VITAL_SIGNS PRIMARY KEY (PERSON_ID, MEASURE_DATE, MEASURE_TIME)
);

IF OBJECT_ID('tempdb.dbo.#temp','U') IS NOT NULL DROP TABLE #temp;
select 
PERSON_ID = i.[CHORDS ID],
MEASURE_DATE = CONVERT(date,v1.admin_date_actual),
MEASURE_TIME = CONVERT(time,v1.admin_time_actual_h),
ENC_ID = NULL,
ENC_TYPE = NULL,
HT_RAW = v1.reading_value,
WT_RAW = v2.reading_value,
HT = v1.reading_entry,
WT = v2.reading_entry,
BMI_RAW = v3.reading_entry,
DIASTOLIC = v4.reading_entry,
SYSTOLIC = v5.reading_entry,
DIASTOLIC_RAW = v4.reading_entry,
SYSTOLIC_RAW = v5.reading_entry,
BP_TYPE = NULL,
POSITION = NULL,
HEAD_CIR_RAW = v6.reading_entry,
RESPIR_RAW = v7.reading_entry,
TEMP_RAW = v9.reading_entry,
PULSE_RAW = v8.reading_entry
into #temp
from avatardw.system.cw_vital_signs v1
LEFT JOIN avatardw.system.cw_vital_signs v2 on v1.PATID = v2.PATID and v1.Unique_Row_Id = v2.Unique_Row_Id and v2.vital_sign = 'Weight (lbs)' and CONVERT(float,v2.reading_entry) < 1000
LEFT JOIN avatardw.system.cw_vital_signs v3 on v1.PATID = v3.PATID and v1.Unique_Row_Id = v3.Unique_Row_Id and v3.vital_sign = 'BMI' and CONVERT(float,v3.reading_entry) <= 100
LEFT JOIN avatardw.system.cw_vital_signs v4 on v1.PATID = v4.PATID and v1.Unique_Row_Id = v4.Unique_Row_Id and v4.vital_sign = 'Blood Pressure Dist' and CONVERT(float,v4.reading_entry) <= 500
LEFT JOIN avatardw.system.cw_vital_signs v5 on v1.PATID = v5.PATID and v1.Unique_Row_Id = v5.Unique_Row_Id and v5.vital_sign = 'Blood Pressure Sys' and CONVERT(float,v5.reading_entry) <= 500
LEFT JOIN avatardw.system.cw_vital_signs v6 on v1.PATID = v6.PATID and v1.Unique_Row_Id = v6.Unique_Row_Id and v6.vital_sign = 'Head Circumference (in)'
LEFT JOIN avatardw.system.cw_vital_signs v7 on v1.PATID = v7.PATID and v1.Unique_Row_Id = v7.Unique_Row_Id and v7.vital_sign = 'Respiration'
LEFT JOIN avatardw.system.cw_vital_signs v8 on v1.PATID = v8.PATID and v1.Unique_Row_Id = v8.Unique_Row_Id and v8.vital_sign = 'Pulse'
LEFT JOIN avatardw.system.cw_vital_signs v9 on v1.PATID = v9.PATID and v1.Unique_Row_Id = v9.Unique_Row_Id and v9.vital_sign = 'Temp (F)'
LEFT JOIN staging.chords.identifiers i on i.[Consumer ID] = CONVERT(int,v1.PATID)
where v1.vital_sign = 'Height (in)' and i.[CHORDS ID] is not null and v1.admin_date_actual >= '10/1/2015' 
;

insert into #vital_signs
SELECT
t.PERSON_ID    ,
t.MEASURE_DATE ,
t.MEASURE_TIME ,
ENC_ID       ,
ENC_TYPE      ,
HT_RAW       ,
WT_RAW       ,
HT ,
WT   ,
BMI_RAW      ,
DIASTOLIC    ,
SYSTOLIC     ,
DIASTOLIC_RAW,
SYSTOLIC_RAW ,
BP_TYPE      ,
POSITION     ,
HEAD_CIR_RAW ,
RESPIR_RAW   ,
TEMP_RAW     ,
PULSE_RAW    
FROM #temp t
LEFT JOIN (
select PERSON_ID, MEASURE_DATE, MEASURE_TIME
from #temp
GROUP BY PERSON_ID, MEASURE_DATE, MEASURE_TIME
HAVING COUNT(*) > 1) t2 on t.PERSON_ID = t2.PERSON_ID and t.MEASURE_DATE = t2.MEASURE_DATE and t.MEASURE_TIME = t2.MEASURE_TIME
where t2.PERSON_ID is null
;


IF OBJECT_ID('dbo.VITAL_SIGNS','U') IS NOT NULL TRUNCATE TABLE dbo.VITAL_SIGNS;

insert into dbo.VITAL_SIGNS
SELECT *
FROM #VITAL_SIGNS
;

