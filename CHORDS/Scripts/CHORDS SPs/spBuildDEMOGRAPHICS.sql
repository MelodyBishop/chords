USE [CHORDSlive]
GO
/****** Object:  StoredProcedure [dbo].[spBuildDEMOGRAPHICS]    Script Date: 9/14/2017 10:26:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spBuildDEMOGRAPHICS]
AS

/* 
=================================================
Object Name...: spBuildDEMOGRAPHICS
Purposes......: Build CHORDS VDW P1 Table DEMOGRAPHICS
Author........: CJ McKinney
Create date...: 2017-5-30

Modified 2017-9-10 by CJ McKinney
Updated to conform to VDW 3.1 specifications
*/

IF OBJECT_ID('tempdb.dbo.#DEMOGRAPHICS','U') IS NOT NULL DROP TABLE #DEMOGRAPHICS;
CREATE TABLE #DEMOGRAPHICS
(
PERSON_ID           CHAR(12) NOT NULL PRIMARY KEY,
MRN                 Char(9) NOT NULL,
BIRTH_DATE          DATE NOT NULL,
GENDER              Char(1) NOT NULL DEFAULT 'U',
PRIMARY_LANGUAGE    nvarCHAR(3) NOT NULL DEFAULT 'und',
NEEDS_INTERPRETER   CHAR(1) NOT NULL DEFAULT 'U', 
RACE1               Char(2) NOT NULL,
RACE2               Char(2) NOT NULL,
RACE3               Char(2) NOT NULL,
RACE4               Char(2) NOT NULL,
RACE5               Char(2) NOT NULL,    
HISPANIC            Char(1) NOT NULL,
SEXUAL_ORIENTATION  nvarChar(2) NOT NULL DEFAULT 'UN',
GENDER_IDENTITY  nvarChar(2) NOT NULL DEFAULT 'UN',
);

IF OBJECT_ID('tempdb.do.#temp','U') IS NOT NULL DROP TABLE #temp; 
SELECT
PERSON_ID = i.[CHORDS ID],
MRN = PATID,
BIRTH_DATE = date_of_birth,
GENDER = sex_code,
PRIMARY_LANGUAGE = CASE WHEN primary_language_code = '01' THEN 'eng'  
						WHEN primary_language_code = '02' THEN 'spa' 
						ELSE 'und' 
						END,
NEEDS_INTERPRETER = 'U', --Need to find this in Avatar
RACE1 =   CASE
			 WHEN SUBSTRING(other_race_code,1,2) = '01' THEN 'IN'
			 WHEN SUBSTRING(other_race_code,1,2) = '02' THEN 'AS'
			 WHEN SUBSTRING(other_race_code,1,2) = '03' THEN 'BA'
			 WHEN SUBSTRING(other_race_code,1,2) = '04' THEN 'HP'
			 WHEN SUBSTRING(other_race_code,1,2) = '05' THEN 'WH'
			 ELSE 'UN'
			 END,
RACE2 =   CASE
			 WHEN SUBSTRING(other_race_code,4,2) = '01' THEN 'IN'
			 WHEN SUBSTRING(other_race_code,4,2) = '02' THEN 'AS'
			 WHEN SUBSTRING(other_race_code,4,2) = '03' THEN 'BA'
			 WHEN SUBSTRING(other_race_code,4,2) = '04' THEN 'HP'
			 WHEN SUBSTRING(other_race_code,4,2) = '05' THEN 'WH'
			 ELSE 'UN'
			 END,
RACE3 =   CASE
			 WHEN SUBSTRING(other_race_code,7,2) = '01' THEN 'IN'
			 WHEN SUBSTRING(other_race_code,7,2) = '02' THEN 'AS'
			 WHEN SUBSTRING(other_race_code,7,2) = '03' THEN 'BA'
			 WHEN SUBSTRING(other_race_code,7,2) = '04' THEN 'HP'
			 WHEN SUBSTRING(other_race_code,7,2) = '05' THEN 'WH'
			 ELSE 'UN'
			 END,
RACE4 =   CASE
			 WHEN SUBSTRING(other_race_code,10,2) = '01' THEN 'IN'
			 WHEN SUBSTRING(other_race_code,10,2) = '02' THEN 'AS'
			 WHEN SUBSTRING(other_race_code,10,2) = '03' THEN 'BA'
			 WHEN SUBSTRING(other_race_code,10,2) = '04' THEN 'HP'
			 WHEN SUBSTRING(other_race_code,10,2) = '05' THEN 'WH'
			 ELSE 'UN'
			 END,
RACE5 =   CASE
			 WHEN SUBSTRING(other_race_code,13,2) = '01' THEN 'IN'
			 WHEN SUBSTRING(other_race_code,13,2) = '02' THEN 'AS'
			 WHEN SUBSTRING(other_race_code,13,2) = '03' THEN 'BA'
			 WHEN SUBSTRING(other_race_code,13,2) = '04' THEN 'HP'
			 WHEN SUBSTRING(other_race_code,13,2) = '05' THEN 'WH'
			 ELSE 'UN'
			 END,   
HISPANIC = CASE WHEN ethnic_origin_code = '1' THEN 'Y' 
				WHEN ethnic_origin_code = '0' THEN 'N' 
				ELSE 'U' 
				END,
SEXUAL_ORIENTATION = 'UN',
GENDER_IDENTITY = 'UN',
ROW_NUMBER() OVER (PARTITION BY PATID ORDER BY data_entry_date DESC) as dRank
into #temp
FROM AvatarDW.SYSTEM.patient_demographic_history pdh
LEFT JOIN Staging.chords.identifiers i on i.[Consumer ID] = pdh.PATID
WHERE date_of_birth is not null
;

insert into #DEMOGRAPHICS
select PERSON_ID, MRN, BIRTH_DATE, GENDER, PRIMARY_LANGUAGE, NEEDS_INTERPRETER, RACE1, RACE2, RACE3, RACE4, RACE5, HISPANIC, SEXUAL_ORIENTATION, GENDER_IDENTITY
from #temp
WHERE dRank = 1 and PERSON_ID is not null
;

IF OBJECT_ID('dbo.DEMOGRAPHICS','U') IS NOT NULL TRUNCATE TABLE dbo.DEMOGRAPHICS;


insert into dbo.DEMOGRAPHICS
select *
from #DEMOGRAPHICS
;
