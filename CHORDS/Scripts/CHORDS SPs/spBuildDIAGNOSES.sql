USE [CHORDSlive]
GO
/****** Object:  StoredProcedure [dbo].[spBuildDIAGNOSES]    Script Date: 9/14/2017 10:26:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spBuildDIAGNOSES]
AS

/* 
=================================================
Object Name...: spBuildDIAGNOSES
Purposes......: Build CHORDS VDW P1 Table DIAGNOSES
Author........: CJ McKinney
Create date...: 2017-5-30

Modified 2017-9-10 by CJ McKinney
Updated to Conform to VDW 3.1 specifications
*/

IF OBJECT_ID('tempdb.dbo.#DIAGNOSES','U') is not null DROP TABLE #DIAGNOSES;
CREATE TABLE #DIAGNOSES
(
ENC_ID              CHAR(36) NOT NULL,
DX                  CHAR(10) NOT NULL,
ADATE               DATE NOT NULL,
DIAGPROVIDER        CHAR(36) NOT NULL,
PERSON_ID           CHAR(12) NOT NULL,
ENCTYPE             CHAR(2) NOT NULL,
PROVIDER            CHAR(36) NOT NULL,
ORIGDX              CHAR(10) NOT NULL,
DX_CODETYPE         CHAR(2) NOT NULL,
PRINCIPAL_DX        CHAR(1) NOT NULL,
PRIMARY_DX          CHAR(1) NOT NULL,


CONSTRAINT pk_DIAGNOSES PRIMARY KEY (ENC_ID, DX, DIAGPROVIDER)
);

IF OBJECT_ID('tempdb.dbo.#temp','U') is not null DROP TABLE #temp;

select DISTINCT
  ENC_ID = s.[Service ID], 
  DX = d.[ICD Code],
  ADATE = s.Date, 
  DIAGPROVIDER = RIGHT([Diagnosing Staff],4),
  PERSON_ID = i.[CHORDS ID],
  ENCTYPE = CASE WHEN [Service Type] like '%residential%' THEN 'IP' WHEN [Mode of Contact] = 'Face-to-Face' THEN 'AV' WHEN [Mode of Contact] = 'Telephone' THEN 'TE' WHEN [Mode of Contact] = 'Written' THEN 'EM' ELSE 'OE' END,
  PROVIDER = RIGHT(s.Staff,4),
  ORIGDX = d.[ICD Code],
  DX_CODETYPE = CASE WHEN d.[ICD Code Type] = 'ICD10' THEN '10' WHEN d.[ICD Code Type] = 'ICD9' THEN '09' ELSE 'UN' END,
  PRINCIPAL_DX = 'X',
  PRIMARY_DX = CASE WHEN [Billing Priority] = 'Primary' THEN 'P' WHEN [Billing Priority] = 'Secondary' THEN 'S' ELSE 'X' END
into #temp
FROM Reporting.dbo.Services s
LEFT JOIN  construct.dbo.[Avatar Diagnosis] d on s.[Consumer ID] = d.[Consumer ID] and s.[Episode Number] = d.[Episode Number] and s.Date between d.[Datetime of Diagnosis] and ISNULL(d.[Diagnosis Lapse Date],GETDATE())
LEFT JOIN  staging.chords.identifiers i on i.[Consumer ID] = s.[Consumer ID]
WHERE s.Date >= '10/1/2015' and [Counts Towards Census?] = 'Yes' and [Contract Compliance Category] <> 'Never Billable' and [Service role] = 'Primary' and d.[ICD Code] is not null
ORDEr BY DATE DESC 
;

insert into #DIAGNOSES
Select t.*
from #temp t
LEFT JOIN (select ENC_ID, DX, DIAGPROVIDER, COUNT(*) as N
from #temp
GROUP BY  ENC_ID, DX, DIAGPROVIDER
) n on t.ENC_ID = n.ENC_ID and t.DX = n.DX and t.DIAGPROVIDER= n.DIAGPROVIDER
where n.N = 1
;

IF OBJECT_ID('dbo.DIAGNOSES','U') is not null TRUNCATE TABLE dbo.DIAGNOSES;


insert into dbo.DIAGNOSES
SELECT *
FROM #DIAGNOSES
;
