USE [CHORDSlive]
GO
/****** Object:  StoredProcedure [dbo].[spBuildCENSUS_LOCATION]    Script Date: 9/14/2017 10:26:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spBuildCENSUS_LOCATION]
AS

/* 
=================================================
Object Name...: spBuildCENSUS_LOCATION
Purposes......: Build CHORDS VDW P1 Table CENSUS_LOCATION
Author........: CJ McKinney
Create date...: 2017-5-30

Modified 2017-9-10 by CJ McKinney
Updated to conform to VDW 3.1 specifications
*/

IF OBJECT_ID('tempdb.dbo.#CENSUS_LOCATION','U') IS NOT NULL DROP TABLE #CENSUS_LOCATION;
CREATE TABLE #CENSUS_LOCATION
(
PERSON_ID               VARCHAR(12) NOT NULL, 
LOC_START               DATE NOT NULL,
LOC_END                 DATE NULL,
GEOCODE                 NVARCHAR(15) NULL, 
CITY_GEOCODE			NVARCHAR(35) NULL,
GEOCODE_BOUNDARY_YEAR   NUMERIC(8) NULL, 
GEOLEVEL                NCHAR(1) NULL, 
MATCH_STRENGTH          NCHAR(1) NULL, 
LATITUDE                NUMERIC(8,6) NULL, 
LONGITUDE               NUMERIC(9,6) NULL, 
GEOCODE_APP             NVARCHAR(50) NULL,
);

IF OBJECT_ID('tempdb.dbo.#temp','U') IS NOT NULL DROp TABLe #temp;
select DISTINCT
PERSON_ID = i.[CHORDS ID],
LOC_START = CONVERT(date, data_entry_date),
LOC_END = NULL,
GEOCODE = CASE WHEN patient_street_1 like '%4141%' and patient_street_1 like '%Dickenson%' THEN '08031' ELSE g.VDW_CENSUS_LOCATION END,
GEOCODE_BOUNDARY_YEAR = NULL,
GEOLEVEL = CASE WHEN LEN(CASE WHEN patient_street_1 like '%4141%' and patient_street_1 like '%Dickenson%' THEN '08031' ELSE g.VDW_CENSUS_LOCATION END) = 11 THEN 'T' ELSE 'C' END,
MATCH_STRENGTH = NULL,
LATITUDE = g.LATITUDE,
LONGITUDE = g.LONGITUDE,
GEOCODE_APP = NULL,
CITY_GEOCODE = UPPER(g.CITY)
into #temp
from avatardw.system.patient_demographic_history d
LEFT JOIN staging.chords.identifiers i on CONVERT(int,d.PATID) = i.[Consumer ID]
LEFt JOIN staging.geo.[Geocoded Addresses] g on d.patient_street_1 = g.Address_1 and d.patient_city = g.CITY and d.patient_state = g.STATE and d.patient_zipcode = CONVERT(varchar(max),g.ZIP)
WHERE g.VDW_CENSUS_LOCATION is not null
ORDEr BY PERSON_ID, LOC_START
;


insert into #CENSUS_LOCATION
select
t.PERSON_ID,
t.LOC_START,
LOC_END = LEAD(t.LOC_START,1) OVER(PARTITION BY t.PERSON_ID ORDER BY t.LOC_START),
GEOCODE,
CITY_GEOCODE,
GEOCODE_BOUNDARY_YEAR,
GEOLEVEL,
MATCH_STRENGTH,
LATITUDE,
LONGITUDE,
GEOCODE_APP
from #temp t
LEFT JOIN
(
select PERSON_ID, LOC_START
from #temp
GROUP BY PERSON_ID, LOC_START
HAVING COUNT(*) > 1
) t2 on t.PERSON_ID = t2.PERSON_ID and t.LOC_START = t2.LOC_START
WHERE t2.PERSON_ID is null and t.PERSON_ID is not null
order by person_id, Loc_start
;

IF OBJECT_ID('dbo.CENSUS_LOCATION','U') IS NOT NULL TRUNCATE TABLE dbo.CENSUS_LOCATION;

insert into dbo.CENSUS_LOCATION
SELECT *
FROM #CENSUS_LOCATION
WHERE PERSON_ID is not null
;

