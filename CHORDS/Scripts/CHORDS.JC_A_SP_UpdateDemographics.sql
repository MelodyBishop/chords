USE CHORDS
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DROP PROCEDURE CHORDS.JC_A_SP_UpdateCHORDS_DEMOGRAPHICS
GO

--Begin transaction
--execute [CHORDS].[JC_A_SP_UpdateCHORDS_DEMOGRAPHICS]
--commit transaction
--rollback transaction 
 --=============================================
-- Author: DAVID G
-- Create date: ~01/11/2018
-- Modified:   
-- Description:    Adds new Provider ID to chords.CHORDS_PROVIDER
-- Moved to CHORDS, updated to leverage history so we can capture latest info for this Level one table, Schema 4/17/2018 - MB

-- =============================================
CREATE PROCEDURE CHORDS.JC_A_SP_UpdateCHORDS_DEMOGRAPHICS
AS
BEGIN

--PER CHORDS DOCUMENTATION
--The DEMOGRAPHICS table contains basic person-descriptive for individuals found in all other VDW tables and serves the traditional �person table� role. 
--The DEMOGRAPHICS table should reflect a patient�s most recent demographic information. 
--If a self-reported demographic attribute, such as race or primary language changes, this should be updated in the DEMOGRAPHICS table. 
--Thus, the DEMOGRAPHICS table should be rerun when the VDW is updated to reflect any changes in patient demographic information.
--One issue that can arise in the DEMOGRAPHICS table is duplicate patients.
--Duplicate patients should be removed/collapsed from the DEMOGRAPHICS table when they are identified in the healthcare organization.
--If an organization has a known problem with duplicates and no standard process to remove them, please note this on the data dictionary.
--Inclusions: Every PERSON_ID appearing in any other VDW file should appear in DEMOGRAPHICS 
--Exclusions: No individuals are excluded from the DEMOGRAPHICS table 


--IF OBJECT_ID('chords.CHORDS_ID','U') is null 
--	CREATE TABLE chords.CHORDS_ID ([MRN] varchar(20), [PERSON_ID] varchar(12));

if OBJECT_ID('tempdb.dbo.#CHORDSpatids','U') is not null DROP table #CHORDSpatids;
--if OBJECT_ID('tempdb.dbo.#clients','U') is not null DROP table #clients;


--Gathers current new PATIDs, why not use Latest entry from history??--129055 distinct patids
SELECT DISTINCT 
	pdh.[patid] 
into 
	#CHORDSpatids
From 
	 AVATARDW.[SYSTEM].patient_demographic_History pdh
WHERE 
	(upper(PDH.patient_name) not like 'TEST%' and upper(pdh.patient_name) not like 'GROUP%')
	--and pdh.[patid]-- collate SQL_Latin1_General_CP1_CI_AS 
	--not in (Select DISTINCT [MRN] FROM chords.CHORDS_ID); ---WHY eXCLUDE, are we just adding here?

--Gather PATID MOST RECENT DEMOGRAPHIS INFORMATION
if OBJECT_ID('tempdb.dbo.#tempDemo','U') is not null DROP table #tempDemo;
Select hist.*
into #TempDemo
From
(Select patid, Max(blank_row_ID) MAXXID from  AVATARDW.[SYSTEM].patient_demographic_History GRoup by patid) pdh 
inner join 
AVATARDW.[SYSTEM].patient_demographic_History hist
on pdh.patid = hist.patid
and hist.blank_row_ID = pdh.MAXXID

--Not sure we need this yet
--SELECT DISTINCT 
--	#CHORDSpatids.[patid], 
--	ROW_NUMBER() OVER (ORDER BY #CHORDSpatids.[patid]) as nRow
--into 
--	#clients
--From 
--	#CHORDSpatids
--WHERE 
--	#CHORDSpatids.[patid] collate SQL_Latin1_General_CP1_CI_AS not in (Select DISTINCT [MRN] FROM chords.CHORDS_ID);

--Declare @CntUp int;
--Declare @consumercount int;
--Declare @chordsid varchar(12);
--Declare @consumerid int;

--set @CntUp = 1;
--set @consumercount = (SELECT COUNT(*) FROM #clients);

--While @CntUp <= @consumercount
--	BEGIN
--		SET @chordsid = 'JC' + RIGHT(CAST(CAST(CAST(NEWID() AS VARBINARY) AS BIGINT) AS VARCHAR(30)),10);
--		SET @consumerid = (SELECT [patid] FRom #clients Where nRow = @CntUp);

--		IF (SELECT [PERSON_ID] FROM chords.CHORDS_ID WHERE [PERSON_ID] = @chordsid) IS NULL
--			BEGIN
--				SET @CntUp = @CntUp + 1;
--				insert into chords.CHORDS_ID ([MRN], [PERSON_ID])
--				SELECT @consumerid, @chordsid
--			END 
--	END

if OBJECT_ID('tempdb.dbo.#CHORDSTemp','U') is not null DROP table #CHORDSTemp;
if OBJECT_ID('tempdb.dbo.#temp_split_present_concern','U')  is not null drop table #temp_split_present_concern;

select 
--	patient_current_demographics.patient_name,
	'JC1234567890' as PERSON_ID,
	patient_current_demographics.patid as MRN,
	patient_current_demographics.date_of_birth as BIRTH_DATE,
	patient_current_demographics.sex_code as GENDER,

case 
	when patient_current_demographics.[primary_language_value] = 'Amharic' then 'AMH'
	when patient_current_demographics.[primary_language_value] = 'Arabic' then 'ARA'
	when patient_current_demographics.[primary_language_value] = 'Cantonese' then 'UNK'
	when patient_current_demographics.[primary_language_value] = 'Cherokee' then 'CHR'
	when patient_current_demographics.[primary_language_value] = 'Chinese' then 'CHI'
	when patient_current_demographics.[primary_language_value] = 'Croatian' then 'HRV'
	when patient_current_demographics.[primary_language_value] = 'Czech' then 'CZE'
	when patient_current_demographics.[primary_language_value] = 'Dutch' then 'DUT'
	when patient_current_demographics.[primary_language_value] = 'English' then 'ENG'
	when patient_current_demographics.[primary_language_value] = 'Farsi' then 'UNK'
	when patient_current_demographics.[primary_language_value] = 'French' then 'FRE'
	when patient_current_demographics.[primary_language_value] = 'German' then 'GER'
	when patient_current_demographics.[primary_language_value] = 'Greek' then 'GRE'
	when patient_current_demographics.[primary_language_value] = 'Hindi' then 'HIN'
	when patient_current_demographics.[primary_language_value] = 'Hmong' then 'HMN'
	when patient_current_demographics.[primary_language_value] = 'Italian' then 'ITA'
	when patient_current_demographics.[primary_language_value] = 'Japanese' then 'JPN'
	when patient_current_demographics.[primary_language_value] = 'Loatian' then 'UNK'
	when patient_current_demographics.[primary_language_value] = 'Mandarin' then 'UNK'
	when patient_current_demographics.[primary_language_value] = 'Mien' then 'UNK'
	when patient_current_demographics.[primary_language_value] = 'No Entry' then 'UNK'
	when patient_current_demographics.[primary_language_value] = 'Other' then 'UNK'
	when patient_current_demographics.[primary_language_value] = 'Other African' then 'UNK'
	when patient_current_demographics.[primary_language_value] = 'Other Communication' then 'UNK'
	when patient_current_demographics.[primary_language_value] = 'Other Filipino' then 'UNK'
	when patient_current_demographics.[primary_language_value] = 'Other Native American' then 'UNK'
	when patient_current_demographics.[primary_language_value] = 'Pennsylvania Du' then 'UNK'
	when patient_current_demographics.[primary_language_value] = 'Polish' then 'POL'
	when patient_current_demographics.[primary_language_value] = 'Rumanian' then 'UNK'
	when patient_current_demographics.[primary_language_value] = 'Russian' then 'RUS'
	when patient_current_demographics.[primary_language_value] = 'Serbian' then 'SRP'
	when patient_current_demographics.[primary_language_value] = 'Sign Language' then 'SGN'
	when patient_current_demographics.[primary_language_value] = 'Spanish' then 'SPA'
	when patient_current_demographics.[primary_language_value] = 'Tagalog' then 'TGL'
	when patient_current_demographics.[primary_language_value] = 'Thai' then 'THA'
	when patient_current_demographics.[primary_language_value] = 'Vietnamese' then 'YIE'
	else 'UNK'
end as PRIMARY_LANGUAGE,
	patient_current_demographics.other_race_code,
	patient_current_demographics.other_race_value_long,
	'U' as NEEDS_INTERPRETER,
	'UN' as RACE1,
	'UN' as RACE2,
	'UN' as RACE3,
	'UN' as RACE4,
	'UN' as RACE5,

	case 
	when patient_current_demographics.[ethnic_origin_code] = '1' then 'N'
	when patient_current_demographics.[ethnic_origin_code] = '2' then 'Y'
	when patient_current_demographics.[ethnic_origin_code] = '3' then 'Y'
	when patient_current_demographics.[ethnic_origin_code] = '4' then 'N'
	when patient_current_demographics.[ethnic_origin_code] = '5' then 'Y'
	when patient_current_demographics.[ethnic_origin_code] = '6' then 'U'
	else 'U'
end as HISPANIC,

	case 
		when patient_current_demographics.[ss_demographics_dict_2_value] = 'Asexual' then 'AS'
		when patient_current_demographics.[ss_demographics_dict_2_value] = 'Bisexual' then 'BI'
		when patient_current_demographics.[ss_demographics_dict_2_value] = 'Declined/Unknown' then 'DC'
		when patient_current_demographics.[ss_demographics_dict_2_value] = 'Demisexual' then 'OT'
		when patient_current_demographics.[ss_demographics_dict_2_value] = 'Gay' then 'GA'
		when patient_current_demographics.[ss_demographics_dict_2_value] = 'Heterosexual' then 'ST'
		when patient_current_demographics.[ss_demographics_dict_2_value] = 'Lesbian' then 'LE'
		when patient_current_demographics.[ss_demographics_dict_2_value] = 'No Entry' then 'UN'
		when patient_current_demographics.[ss_demographics_dict_2_value] = 'Omnisexual' then 'OT'
		when patient_current_demographics.[ss_demographics_dict_2_value] = 'Other (Specify)' then 'OT'
		when patient_current_demographics.[ss_demographics_dict_2_value] = 'Pansexual' then 'OT'
		when patient_current_demographics.[ss_demographics_dict_2_value] = 'Prefer No Label' then 'DC'
		when patient_current_demographics.[ss_demographics_dict_2_value] = 'Queer' then 'QU'
		when patient_current_demographics.[ss_demographics_dict_2_value] = 'Questioning' then 'QS'
		else 'UN'
	end as SEXUAL_ORIENTATION,
	
	'UN' as GENDER_IDENTITY
into #CHORDSTemp
from 
	#tempDemo patient_current_demographics
--where
--	(upper(patient_current_demographics.patient_name) not like 'TEST%' and upper(patient_current_demographics.patient_name) not like 'GROUP%')
--	  and patient_current_demographics.[patid] --collate SQL_Latin1_General_CP1_CI_AS 
--		not in (Select DISTINCT [MRN] FROM [AvatarDW].[CHORDS].[DEMOGRAPHICS]);
--select * from #CHORDSTEMP

;with cte_split_string(MRN, other_race_code, current_race, other_race_value_long, level) as (
   select MRN, other_race_code, 
       LEFT(CAST(other_race_value_long as varchar(max)), CHARINDEX('&',other_race_value_long+'&')-1),
         STUFF(other_race_value_long, 1, CHARINDEX('&',other_race_value_long+'&'), ''), level=1
   from #CHORDSTemp
   union all
   select MRN, other_race_code,
       LEFT(CAST(other_race_value_long as varchar(max)), CHARINDEX('&',other_race_value_long+'&')-1),
        STUFF(other_race_value_long, 1, CHARINDEX('&',other_race_value_long+'&'), ''),cte_split_string.level+1
   from cte_split_string
   where other_race_value_long > ''
)

select MRN, other_race_code, current_race, other_race_value_long, level
into #temp_split_present_concern
from cte_split_string ct
order by MRN, level
option (maxrecursion 0);
--select * from #temp_split_present_concern order by 1

update c
set c.RACE1 =
case 
	when t.current_race = 'Native Hawaiian' then 'HP'
	when t.current_race = 'Other Pacific Islander' then 'HP'
	when t.current_race = 'Samoan' then 'HP'
    when t.current_race = 'Guamanian/Chamorro' then 'HP'
    when t.current_race = 'American Indian/Alaska Native' then 'IN'
    when t.current_race = 'Vietnamese' then 'AS'
    when t.current_race = 'Japanese' then 'AS'
    when t.current_race = 'Chinese' then 'AS'
    when t.current_race = 'Korean' then 'AS'
    when t.current_race = 'Asian Indian' then 'AS'
    when t.current_race = 'Other Asian' then 'AS'
    when t.current_race = 'White/Caucasian' then 'WH'
    when t.current_race = 'Black/African-American' then 'BA'
	else 'UN' 
end 
from #CHORDSTemp c
	inner join  #temp_split_present_concern t on t.mrn = c.MRN and t.level = 1;

update c
set c.RACE2 =
case 
	when t.current_race = 'Native Hawaiian' then 'HP'
	when t.current_race = 'Other Pacific Islander' then 'HP'
	when t.current_race = 'Samoan' then 'HP'
    when t.current_race = 'Guamanian/Chamorro' then 'HP'
    when t.current_race = 'American Indian/Alaska Native' then 'IN'
    when t.current_race = 'Vietnamese' then 'AS'
    when t.current_race = 'Japanese' then 'AS'
    when t.current_race = 'Chinese' then 'AS'
    when t.current_race = 'Korean' then 'AS'
    when t.current_race = 'Asian Indian' then 'AS'
    when t.current_race = 'Other Asian' then 'AS'
    when t.current_race = 'White/Caucasian' then 'WH'
    when t.current_race = 'Black/African-American' then 'BA'
	else 'UN' 
end 
from #CHORDSTemp c
	inner join  #temp_split_present_concern t on t.mrn = c.MRN and t.level = 2;

update c
set c.RACE3 =
case 
	when t.current_race = 'Native Hawaiian' then 'HP'
	when t.current_race = 'Other Pacific Islander' then 'HP'
	when t.current_race = 'Samoan' then 'HP'
    when t.current_race = 'Guamanian/Chamorro' then 'HP'
    when t.current_race = 'American Indian/Alaska Native' then 'IN'
    when t.current_race = 'Vietnamese' then 'AS'
    when t.current_race = 'Japanese' then 'AS'
    when t.current_race = 'Chinese' then 'AS'
    when t.current_race = 'Korean' then 'AS'
    when t.current_race = 'Asian Indian' then 'AS'
    when t.current_race = 'Other Asian' then 'AS'
    when t.current_race = 'White/Caucasian' then 'WH'
    when t.current_race = 'Black/African-American' then 'BA'
	else 'UN' 
end 
from #CHORDSTemp c
	inner join  #temp_split_present_concern t on t.mrn = c.MRN and t.level = 3;

update c
set c.RACE4 =
case 
	when t.current_race = 'Native Hawaiian' then 'HP'
	when t.current_race = 'Other Pacific Islander' then 'HP'
	when t.current_race = 'Samoan' then 'HP'
    when t.current_race = 'Guamanian/Chamorro' then 'HP'
    when t.current_race = 'American Indian/Alaska Native' then 'IN'
    when t.current_race = 'Vietnamese' then 'AS'
    when t.current_race = 'Japanese' then 'AS'
    when t.current_race = 'Chinese' then 'AS'
    when t.current_race = 'Korean' then 'AS'
    when t.current_race = 'Asian Indian' then 'AS'
    when t.current_race = 'Other Asian' then 'AS'
    when t.current_race = 'White/Caucasian' then 'WH'
    when t.current_race = 'Black/African-American' then 'BA'
	else 'UN' 
end 
from #CHORDSTemp c
	inner join  #temp_split_present_concern t on t.mrn = c.MRN and t.level = 4;

update c
set c.RACE5 =
case 
	when t.current_race = 'Native Hawaiian' then 'HP'
	when t.current_race = 'Other Pacific Islander' then 'HP'
	when t.current_race = 'Samoan' then 'HP'
    when t.current_race = 'Guamanian/Chamorro' then 'HP'
    when t.current_race = 'American Indian/Alaska Native' then 'IN'
    when t.current_race = 'Vietnamese' then 'AS'
    when t.current_race = 'Japanese' then 'AS'
    when t.current_race = 'Chinese' then 'AS'
    when t.current_race = 'Korean' then 'AS'
    when t.current_race = 'Asian Indian' then 'AS'
    when t.current_race = 'Other Asian' then 'AS'
    when t.current_race = 'White/Caucasian' then 'WH'
    when t.current_race = 'Black/African-American' then 'BA'
	else 'UN' 
end 
from #CHORDSTemp c
	inner join  #temp_split_present_concern t on t.mrn = c.MRN and t.level = 5;

--select * from #CHORDSTEMP
--This is no longer needed, we gather person_id above
--update c
--	set c.PERSON_ID = cid.PERSON_ID
--from #CHORDSTemp c
--	inner join [CHORDS].[CHORDS_ID] cid on cid.MRN = c.MRN;

insert into STAGING.[DEMOGRAPHICS]
select 
	id.PERSON_ID,
	c.MRN,
	BIRTH_DATE,
	GENDER,
	PRIMARY_LANGUAGE,
	NEEDS_INTERPRETER,
	RACE1,
	RACE2,
	RACE3,
	RACE4,
	RACE5,
	HISPANIC,
	SEXUAL_ORIENTATION,
	GENDER_IDENTITY
from #CHORDSTemp c
inner join CHORDS.CHORDS_ID id
on c.mrn = id.mrn
		
--select * from #CHORDSTemp;
END