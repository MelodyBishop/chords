

USE [CHORDS]
GO


DROP TABLE STAGING.Encounters
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE STAGING.Encounters (
	[ENC_ID] [nvarchar](36) NOT NULL,
	[PERSON_ID] [nvarchar](12) NOT NULL,
	[ADATE] [date] NOT NULL,
	[DDATE] [date] NULL,
	[PROVIDER] [nvarchar](36) NOT NULL,
	[ENC_COUNT] [numeric](3, 0) NOT NULL,
	[DRG_VERSION] [nchar](1) NULL,
	[DRG_VALUE] [nvarchar](4) NULL,
	[ENCTYPE] [nvarchar](2) NOT NULL,
	[ENCOUNTER_SUBTYPE] [nvarchar](2) NOT NULL,
	[FACILITY_CODE] [nvarchar](36) NOT NULL,
	[DISCHARGE_DISPOSITION] [nchar](1) NOT NULL,
	[DISCHARGE_STATUS] [nvarchar](2) NOT NULL,
	[ADMITTING_SOURCE] [nvarchar](2) NULL,
	[DEPARTMENT] [nvarchar](4) NULL,
 CONSTRAINT [PK_CHORDS_ENCOUNTERS] PRIMARY KEY CLUSTERED 
(
	[ENC_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
