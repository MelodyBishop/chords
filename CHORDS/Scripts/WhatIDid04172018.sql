
Scope: 
Chords_ID--
Encounters
Demographics
Diagnosis
Vitals
Census Location -- last geo coded
Demographics
----------------------------------------------------------------

--Table from CHORDS that we can use based on 2010 Census
Census_Demog - From CHORDS - we could join with this for 2010 census information ie avg household income 


Moved and updated/changed 3 stored procedures to CHORDS DB
UpdateDiagnosis from AvatarDW
UpdateCHORDS_Provider from AvatarDW to CHORDS
UpdateCHORDSIDS


Moved table from Tier on Tiercluster to Tier on JCVS2

Renamed table in AvatarDW.AvatarDW.Chords tables
[CHORDS].[CHORDS_ID_zzzMB]
[CHORDS].[DEMOGRAPHICS_obs]
[CHORDS].[RACE_obs]
[CHORDS].[RACETemp_obs]


Imported data from these tables from AvatarDW - ARE THESE STATIC?, or do we need to truncate/reimport periodically?
[dbo].[JC_A_TL_LK_Crosswalk_AvatarTIER_Location]
extracted once, but we may need to know where Mary/Ben will build this in the future
JCMH.DBO.[JCMH__Service_Avatar_research] and Service_Tier


--looks like after we geo code our data we can use the Census_Demog to determine a few things like average income(?)

--created table ZipcodePrimaryLongLat from Zipcode geo code database extracted from web


--left off on Vital Signs and the complication that appears to be no link from vitals to any encounter
