--use CHORDS
--go

--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

----/* ************************************************************************************************************************************************************************************************************************************************ 
----Name:		    JC_A_SP_Create_CHORDS_Encounter_TIER
----Author:		Mary Steffeck, JCMH
----Created:	    March 2018
----Description:  This proc will read all chargeslips in TIER and create CHORDS encounters.
----              
----Modified:     
----
----
----*************************************************************************************************************************************/
--CREATE  procedure CHORDS.[JC_A_SP_Create_CHORDS_Encounter_TIER]
--as

USE DATAMARTS
declare @begindate datetime
set @begindate = '10/1/2015'

select top 10
       (select top 1 person_id
        from CHORDS.CHORDS.CHORDS_ID 
        where MRN = cs.clientkey) as person_id,

       cs.clientkey, cs.datesvc, 
	   replace(convert(varchar(10),cs.datesvc,112),'/','') as adate,  -- format to MMDDYYYY
       cs.chargekey,  --

       NULL as ddate,
       cs.staffkey,  -- need chords version using employee id
       1 as enc_count,
       NULL as drg_version, NULL as drg_value,
       case
         when cs.prockey in (1934,2222,2294,2373,2186,2524,29372722,2330) then 'TE'  -- phone procedures
         else 'OE'  -- other encounter
       end as enctype,
       'OT' as encounter_subtype,  -- other non-hospital
       ' ' as facility_code
	   , cs.PlaceOfSvcLU
	   , (select top 1 ps.Avatar_Loc_Code
		  from Feeds.dbo.JC_A_TL_LK_Crosswalk_AvatarTIER_Location ps --dbo.BLU_PlaceOfService ps
		   where ps.TIER_POSCode = cs.PlaceOfSvcLU) as PlaceOfService,   -- need to crosswalk to Avatar

       'U' as discharge_disposition,
       'UN' as discharge_status,
       'UN' as admitting_source,
       'MH' as department    
-- 
from Tier.dbo.bil_chargeslips cs
inner join  Tier.dbo.T4W_DOCUMENTS d
    on cs.op__docid = d.op__id
inner join  Tier.dbo.fd__clients b
    on cs.clientkey = b.clientkey

where (cs.voidindicator is null or (cs.voidindicator = 'V' and cs.OriginalCS = 0))  -- get only initial charge
  and cs.IsEncounter = 'Y'
  and d.OP__STATUSORD = 5  -- final save only
  --and CAST(CONVERT(char(8), d.op__creationdatetime, 112) AS DATETIME) >= @begindate 
 and substring(b.namel,1,4) <> 'test'  -- omit clients with test of error as last name
 and substring(b.namel,1,5) <> 'error'
  and cs.clientkey in (select av.patid
                       from Avatardw.SYSTEM.patient_demographic_history av
                       where av.patid = cs.clientkey)  -- active Avatar clients only
  --and (select top 1 atl.delta
  --     from Tier.dbo.bil_artranslog atl
  --     where atl.chargekey = cs.chargekey
  --       and atl.ardescr = 'Charge'
  --     order by atl.chargekey desc) > 0  -- initial charge amount, billable if charge is greater than 0
                         
