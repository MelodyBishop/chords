USE CHORDS
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DROP PROCEDURE CHORDS.JC_A_SP_UpdateCHORDS_PROVIDER
GO
--
--Begin transaction
--execute [CHORDS].[JC_A_SP_UpdateCHORDS_PROVIDER]
--commit transaction
--rollback transaction 
-- =============================================
-- Author: Ben P
-- Create date: 01/11/2018
-- Modified:   
-- Description:    Adds new Provider ID to chords.CHORDS_PROVIDER
-- Moved to CHORDS, updated Schema 4/17/2018 - MB
-- =============================================
CREATE PROCEDURE CHORDS.JC_A_SP_UpdateCHORDS_PROVIDER
               -- Add the parameters for the stored procedure here
               --@rptText varchar(max) OUTPUT --removing this
AS
BEGIN
               -- SET NOCOUNT ON added to prevent extra result sets from
               -- interfering with SELECT statements.
SET NOCOUNT ON;

IF OBJECT_ID('chords.CHORDS_PROVIDER','U') is null 
               CREATE TABLE chords.CHORDS_PROVIDER ([Staff_ID] varchar(20), [PROVIDER] varchar(12));

if OBJECT_ID('tempdb.dbo.#CHORDSprovider','U') is not null DROP table #CHORDSprovider;
if OBJECT_ID('tempdb.dbo.#PROVIDER','U') is not null DROP table #PROVIDER;

SELECT DISTINCT 
               staff_member_id 
into 
               #CHORDSprovider
From 
               AvatarDW.SYSTEM.RADplus_users RADplus_users
WHERE 
               (upper(RADplus_users.user_description) not like '%TEST%' 
			   and upper(RADplus_users.user_description) not like '%GROUP%' 
			   and upper(RADplus_users.user_description) not like '%ZZZ%')
               and RADplus_users.[staff_member_id] collate SQL_Latin1_General_CP1_CI_AS not in (Select DISTINCT [Staff_ID] FROM chords.CHORDS_PROVIDER)
			   and RADplus_users.[staff_member_id] not in (0, 999999, 999998);

SELECT DISTINCT 
               #CHORDSprovider.staff_member_id, 
               ROW_NUMBER() OVER (ORDER BY #CHORDSprovider.[staff_member_id]) as nRow
into           #PROVIDER
From 
               #CHORDSprovider
WHERE 
               #CHORDSprovider.staff_member_id collate SQL_Latin1_General_CP1_CI_AS not in (Select DISTINCT Cast([Staff_ID] as int) FROM chords.CHORDS_PROVIDER);

Declare @CntUp int;
Declare @providercount int;
Declare @chordsid varchar(12);
Declare @consumerid int;

set @CntUp = 1;
set @providercount = (SELECT COUNT(*) FROM #PROVIDER);

While @CntUp <= @providercount
               BEGIN
                              SET @chordsid = 'JC' + RIGHT(CAST(CAST(CAST(NEWID() AS VARBINARY) AS BIGINT) AS VARCHAR(30)),13);
                              SET @consumerid = (SELECT [staff_member_id] FRom #PROVIDER Where nRow = @CntUp);

                              IF (SELECT [PROVIDER] FROM chords.CHORDS_PROVIDER WHERE [PROVIDER] = @chordsid) IS NULL
                                             BEGIN
                                                            SET @CntUp = @CntUp + 1;
                                                            insert into chords.CHORDS_PROVIDER ([Staff_ID], [PROVIDER])
                                                            SELECT @consumerid, @chordsid
                                             END 
               END

--select * from chords.CHORDS_PROVIDER

END
GO


