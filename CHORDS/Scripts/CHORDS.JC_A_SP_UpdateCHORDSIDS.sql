USE CHORDS
GO
DROP PROCEDURE CHORDS.[JC_A_SP_UpdateCHORDSIDS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Begin Transaction
--execute [CHORDS].[JC_A_SP_UpdateCHORDSIDS]
--commit transaction
--rollback transaction 

-- =============================================
-- Author:		David R. Gray
-- Create date: 12/28/2017
-- Modified:    12/28/2017 - David R. Gray - Added logging
-- Description:	Adds new CHORDS ID to chords.CHORDS_ID
-- Moved to CHORDS, schema change 4/17/2018 - MB
-- =============================================
CREATE PROCEDURE CHORDS.[JC_A_SP_UpdateCHORDSIDS]
	-- Add the parameters for the stored procedure here
	--@rptText varchar(max) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


IF OBJECT_ID('chords.CHORDS_ID','U') is null 
	CREATE TABLE chords.CHORDS_ID ([MRN] varchar(20), [PERSON_ID] varchar(12));

if OBJECT_ID('tempdb.dbo.#CHORDSpatids','U') is not null DROP table #CHORDSpatids;
if OBJECT_ID('tempdb.dbo.#clients','U') is not null DROP table #clients;


SELECT DISTINCT 
	patient_demographic_history.patid 
into 
	#CHORDSpatids
From 
	AvatarDW.System.patient_demographic_history patient_demographic_history
WHERE 
	(upper(patient_demographic_history.patient_name) not like 'TEST,%' 
 and upper(patient_demographic_history.patient_name) not like 'GROUP%'
 and upper(patient_demographic_history.patient_name) not like 'ZZZ%')
	and patient_demographic_history.patid collate SQL_Latin1_General_CP1_CI_AS not in (Select DISTINCT [MRN] FROM chords.CHORDS_ID);


SELECT DISTINCT 
	#CHORDSpatids.[patid], 
	ROW_NUMBER() OVER (ORDER BY #CHORDSpatids.[patid]) as nRow
into 
	#clients
From 
	#CHORDSpatids
WHERE 
	#CHORDSpatids.patid collate SQL_Latin1_General_CP1_CI_AS not in (Select DISTINCT [MRN] FROM chords.CHORDS_ID);

--Begin Transaction
	
Declare @CntUp int;
Declare @consumercount int;
Declare @chordsid varchar(12);
Declare @consumerid int;

set @CntUp = 1;
set @consumercount = (SELECT COUNT(*) FROM #clients);

While @CntUp <= @consumercount
	BEGIN
		SET @chordsid = 'JC' + RIGHT(CAST(CAST(CAST(NEWID() AS VARBINARY) AS BIGINT) AS VARCHAR(30)),10);
		SET @consumerid = (SELECT patid FRom #clients Where nRow = @CntUp);

		IF (SELECT [PERSON_ID] FROM chords.CHORDS_ID WHERE [PERSON_ID] = @chordsid) IS NULL
			BEGIN
				SET @CntUp = @CntUp + 1;
				insert into chords.CHORDS_ID ([MRN], [PERSON_ID])
				SELECT @consumerid, @chordsid
			END 
	END
--commit transaction
--rollback transaction

END
GO


