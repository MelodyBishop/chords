USE [CHORDSlive]
GO
/****** Object:  StoredProcedure [dbo].[spBuildIdentifiers]    Script Date: 9/14/2017 10:26:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spBuildIdentifiers]
AS

/* 
=================================================
Object Name...: spBuildIdentifiers
Purposes......: Build Table of Unique identifiers for CHORDS VDW
Author........: CJ McKinney
Create date...: 2017-5-30
*/


--IF OBJECT_ID('Staging.chords.identifiers','U') is not null DROP TABLE Staging.chords.identifiers;
--CREATE TABLE Staging.chords.identifiers ([Consumer ID] int, [CHORDS ID] varchar(12));
if OBJECT_ID('tempdb.dbo.#consumers','U') is not null DROP table #consumers;
SELECT DISTINCT [Consumer ID], ROW_NUMBER() OVER (ORDER BY [Consumer ID]) as nRow
into #consumers
From Reporting.dbo.[Consumer Summary]
WHERE [Consumer ID] not in (Select DISTINCT [Consumer ID] FROM Staging.chords.identifiers)
;

Declare @CntUp int = 1;
Declare @consumercount int = (SELECT COUNT(*) FROM #consumers);
Declare @chordsid varchar(12);
Declare @consumerid int;

While @CntUp <= @consumercount
	BEGIN

		SET @chordsid = 'MH' + RIGHT(CAST(CAST(CAST(NEWID() AS VARBINARY) AS BIGINT) AS VARCHAR(30)),10);
		SET @consumerid = (SELECT [Consumer ID] FRom #consumers Where nRow = @CntUp);


		IF (SELECT [CHORDS ID] FROM Staging.chords.identifiers WHERE [CHORDS ID] = @chordsid) IS NULL
			BEGIN
				SET @CntUp = @CntUp + 1;
				insert into Staging.chords.identifiers ([Consumer ID], [CHORDS ID])
				SELECT @consumerid, @chordsid
						END 
	END

-- RIGHT(CAST(CAST(CAST(NEWID() AS VARBINARY) AS BIGINT) AS VARCHAR(30)),9) as [ID]