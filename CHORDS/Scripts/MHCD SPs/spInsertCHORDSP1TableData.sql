USE [CHORDSlive]
GO
/****** Object:  StoredProcedure [dbo].[spInsertCHORDSP1TableData]    Script Date: 9/14/2017 10:26:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spInsertCHORDSP1TableData]
AS

/* 
=================================================
Object Name...: spInsertCHORDSData
Purposes......: Add data to all CHORDS table
Author........: CJ McKinney
Create date...: 2017-9-10

This can be run with a complete VDW rebuild or just itself to update the data in the VDW tables

*/

EXEC dbo.spBuildIdentifiers;
EXEC dbo.spBuildCENSUS_LOCATION;
EXEC dbo.spBuildDEMOGRAPHICS;
EXEC dbo.spBuildDIAGNOSES;
EXEC dbo.spBuildENCOUNTERS;
EXEC dbo.spBuildVITAL_SIGNS;



